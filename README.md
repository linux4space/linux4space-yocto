# Linux4Space Yocto

Linux4Space is a collaborative open source project founded with the intention of creating an open source Yocto based Linux distribution suitable for space applications. The project brings together all the stakeholders: the software and hardware developers, the suppliers, and technology companies. Linux4Space is designed to be compliant with ESA (European Space Agency) Standards and it is based on community defined requirements.

## About repository
This repository serves as a simple Linux4space distro project, which is prepared to be cloned, built and tested on the qemu machine or BeableBone.

The repository contains reference to 3 repositories:

- meta-linux4space - Distro core Linux4Space Layer
- meta-linux4space-apps - Linux4Space application layer
- poky - Reference yocto distro layer

## How to start
The following section describes how you can clone and try this distro on your own.

The first part is cloning. To clone repositories with all submodules, use this command.
```
    git clone https://gitlab.com/linux4space/linux4space-yocto
    cd linux4space-yocto
    git submodule init
    git submodule update
```

After that, the following command will setup environment and add meta-linux4space to your bblayer.conf
'''
    . ./poky/oe-init-build-env
    bitbake-layers add-layer ../meta-linux4space
    bitbake-layers add-layer ../meta-linux4space-apps
'''

In your local.conf change preferred distro to linux4space.

Now we are ready to run bitbake and run qemu.
```
    bitbake image-core-minimal
    # After several hours, we are almost done the previous task, so we can
    # try the linux4space distro using the run emu command.
    runqemu
```

## Mailing list
If you have any question, please feel free to contact us by using mailing list.
More information can be found here https://wiki.linux4space.org/wiki/Mailing_List